import Vue from "vue";
import Router from "vue-router";

import App from "./views/app/App";
import Help from "./views/app/Help";
import Profile from "./views/app/Profile";
import Home from "./views/app/Home";
import BadParked from "./views/app/BadParked";

import SplashScreen from "./views/SplashScreen";

import OnBoarding from "./views/OnBoarding";
import Screen1 from "./components/onboarding/1";
import Screen2 from "./components/onboarding/2";
import Screen3 from "./components/onboarding/3";

Vue.use(Router);

export default new Router({
  routes: [
    { path: "/", redirect: "/loader" },
    { path: "/loader", component: SplashScreen },
    {
      path: "/onboarding",
      component: OnBoarding,
      children: [
        { path: "", redirect: "1" },
        { path: "1", component: Screen1 },
        { path: "2", component: Screen2 },
        { path: "3", component: Screen3 }
      ]
    },
    {
      path: "/app",
      component: App,
      children: [
        { path: "", component: Home },
        { path: "help", component: Help },
        { path: "profile", component: Profile },
        { path: "bad-parked", component: BadParked }
      ]
    }
  ]
});
